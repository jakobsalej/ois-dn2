function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}
 
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
 
function divElementHtmlTekstBold(sporocilo) {
  return $('<div></div>').html('<i style ="font-weight: bold">' + sporocilo + '</i>');
}
 
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  var stetje = sporocilo.split(" ");
  var zacasna2 = sporocilo.substring(0, 12).toLowerCase();
  var pridruzitevSporocilo;
  
  var zasebnoSporocilo;
  
  
  var zacasna = sporocilo.substring(0, 8).toLowerCase();
  
  if(stetje.length > 2 && zacasna2 == "/pridruzitev"){
    pridruzitevSporocilo = klepetApp.pridruzitevGeslo(sporocilo);           //pridruzitevGeslo --> klepet.js
    $('#sporocila').append(divElementHtmlTekst(pridruzitevSporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  
  
  
  
  


 
 

  

  else if(zacasna == "/zasebno"){
    zasebnoSporocilo = klepetApp.posljiZasebnoSporocilo(sporocilo);           //zasebno sporocilo --> klepet.js
    $('#sporocila').append(divElementHtmlTekst(zasebnoSporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  


  else if (sporocilo.charAt(0) == '/') {

    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

   
    
    sporocilo = sporocilo.replace(/</g, "&#60");
    sporocilo = sporocilo.replace(/>/g, "&#62");
    sporocilo = sporocilo.split(";)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
    sporocilo = sporocilo.split(":)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
    sporocilo = sporocilo.split("(y)").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
    sporocilo = sporocilo.split(":*").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
    sporocilo = sporocilo.split(":(").join('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');
 
   
    klepetApp.posljiSporocilo(ime_kanal, sporocilo);
    $('#sporocila').append(divElementHtmlTekstBold(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));

    
  }
 
  $('#poslji-sporocilo').val('');
}
 
var socket = io.connect();

var trenutni_k;


 


var ime;            //vzdevek uporabnika
var ime_kanal;      //ime kanala


var tren_vzdevek;


$(document).ready(function() {
  var klepetApp = new Klepet(socket);
 
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';

      ime = rezultat.vzdevek;                                       //shranim izbran vzdevek za uporabo v metodi "pridruzitevOdgovor"
      $('#kanal').text(ime + " @ " + ime_kanal);                    //dodamo to vrstico, da se naslov spremeni tudi ob menjavi vzdevka

      tren_vzdevek = rezultat.vzdevek;

    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    
  });
 
  socket.on('pridruzitevOdgovor', function(rezultat) {

    
    $('#kanal').text(ime + " @ " + rezultat.kanal);                   //spreminjam tole vrstico (dodam spremenljivko ime)
    ime_kanal = rezultat.kanal;                                       //shranim ime kanala za uporabo v metodi "vzdevekSpremembaOdgovor"
    trenutni_k = rezultat.kanal;

    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    socket.emit("povejKanal", trenutni_k);
  });
 
  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  
  
  
  
  
  socket.on('odgovorZahtevaGeslo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo);
    $('#sporocila').append(novElement);
  });
  
  
  
  
  
  

  
  
  socket.on('privatnoSporocilo2', function (sporocilo2) {
    if(sporocilo2.uporabnik.indexOf(tren_vzdevek) === 0){
      var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo2.besedilo);
      $('#sporocila').append(novElement);
    }
  });
  
  socket.on('privatnoSporocilo3', function (sporocilo2) {
    
      var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo2.besedilo);
      $('#sporocila').append(novElement);
    
  });
  
  


  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();
 
    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }
 
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

 

  
  
 
 socket.on("uporabnikiNaK", function(uporabniki) {
   if(uporabniki.stevec == 1){
      $('#seznam-uporabnikov').empty();
   }
   $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki.vzdevek));
   
 });



  
  


  setInterval(function() {
    socket.emit('kanali');
    
    socket.emit("povejKanal", trenutni_k);
  }, 1000);

 

  
  

  $('#poslji-sporocilo').focus();
 
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});