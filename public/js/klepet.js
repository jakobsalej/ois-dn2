var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


Klepet.prototype.posljiZasebnoSporocilo = function(besedilo) {
  var besede = besedilo.split(' ');
  var sporocilo;
  besede.shift();
  var zac1  = besede[0].substring(0, besede[0].length);
  besede.shift();
  besede = besede.join(" ");
  var zac2 = besede;
  
     // return sporocilo;
  
  if(zac1.charAt(0) == '"' && zac1.charAt(zac1.length-1) == '"'){            // 
    var posljiUporabniku = zac1.substring(1, zac1.length-1);
    
    
    
    if(zac2.charAt(0) == '"' && zac2.charAt(zac2.length-1) == '"'){
      var sporociloUporabniku2 = zac2.substring(1, zac2.length-1);
      var privatnoSporocilo = {
        
        uporabnik: posljiUporabniku,
        sporocilo: sporociloUporabniku2
      };
      
     // return posljiUporabniku + " " + sporociloUporabniku2;
      
      this.socket.emit('privatnoSporocilo', privatnoSporocilo);
      
      //return posljiUporabniku + " " + sporociloUporabniku2;
    }
    else{
      sporocilo = 'Neznan ukaz.';
      return sporocilo;
    }
    
  }
  else{
      sporocilo = 'Neznan ukaz.';
      return sporocilo;
  }
  
  

};


Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};






Klepet.prototype.pridruzitevGeslo = function(besedilo) {
  var besede = besedilo.split(' ');
  var sporocilo;
  besede.shift();
  var zac1  = besede[0].substring(0, besede[0].length);
  besede.shift();
  besede = besede.join(" ");
  var zac2 = besede;
  
     // return sporocilo;
  
  if(zac1.charAt(0) == '"' && zac1.charAt(zac1.length-1) == '"'){            // 
    var posljiUporabniku = zac1.substring(1, zac1.length-1);
    
    
    
    if(zac2.charAt(0) == '"' && zac2.charAt(zac2.length-1) == '"'){
      var sporociloUporabniku2 = zac2.substring(1, zac2.length-1);
      var pridruzitevGeslo = {
        
        kanal: posljiUporabniku,
        geslo: sporociloUporabniku2
      };
      
      //return posljiUporabniku + " " + sporociloUporabniku2;
      
      this.socket.emit('pridruzitevZahtevaGeslo', pridruzitevGeslo);
      
      //return posljiUporabniku + " " + sporociloUporabniku2;
    }
    else{
      sporocilo = 'Neznan ukaz.';
      return sporocilo;
    }
    
  }
  else{
      sporocilo = 'Neznan ukaz.';
      return sporocilo;
  }
  
  

};








Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
      
   /* case 'zasebno':
      var toUser =  besede[1].substring(1, besede[1].length).toLowerCase();
      besede.shift();
      var privatSporocilo = besede.join(' ');
      var privatnoSporocilo = {
        uporabnik: toUser,
        besedilo: privatSporocilo
      };
      this.socket.emit('privatnoSporocilo', privatnoSporocilo);
      break; */
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};