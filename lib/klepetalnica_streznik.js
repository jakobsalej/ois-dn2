var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliGesla = [];
var gesla = [];
var stevnik = 1;

kanaliGesla.push("Skedenj");                       //dodam novkanal v tabelo kanalov!!! 
gesla.push("pogresamluizasuareza88");



exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    
    
    
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjePrivatnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);

    
   //uporabnikiKanal(socket, trenKanal);

    socket.on("povejKanal", function(trenutni_k){
      uporabnikiKanal(socket, trenutni_k);
    });
   

    obdelajPridruzitevKanalu(socket, kanaliGesla, gesla);
    obdelajPridruzitevKanaluGeslo(socket, kanaliGesla, gesla);
    
    

    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};


function uporabnikiKanal(socket, trenutniKanal){
  var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal);
 
    var n = 0;
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      n++;
      socket.emit('uporabnikiNaK', {vzdevek: vzdevkiGledeNaSocket[uporabnikSocketId], 
                                    stevec: n});
       
      
    }
   
    
    
  
}




  








function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  //uporabnikiKanal(socket, trenutniKanal);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  
  
  
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    //uporabnikiKanal(socket, trenutniKanal);
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
  //uporabnikiKanal(socket, trenutniKanal);
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}



function obdelajPosredovanjePrivatnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  
    socket.on('privatnoSporocilo', function (privSporocilo) {
      var text = privSporocilo.sporocilo;
      var upor = privSporocilo.uporabnik;
      
      
      if(uporabljeniVzdevki.indexOf(upor) !== -1 && vzdevkiGledeNaSocket[socket.id].indexOf(upor) !== 0){
         socket.broadcast.emit('privatnoSporocilo2', {
            besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + text,
            uporabnik: upor
         });
         socket.emit('privatnoSporocilo3', {
          besedilo: "(zasebno za " + upor + "): " + text});
      
      }
      
      else{
          socket.emit('privatnoSporocilo3', {
          besedilo: "Sporočila " + text + " uporabniku z vzdevkom " + upor + " ni bilo mogoče posredovati."});
      }
             
    
    
    
    
   
      
      });
  
}




function obdelajPridruzitevKanalu(socket, kanaliGesla, gesla) {

  socket.on('pridruzitevZahteva', function(kanal) {
    
    var stevko = 0;
    var novi = kanal.novKanal;
    
    for(var i in kanaliGesla){
      if(kanaliGesla[i] == novi){
        stevko++;
        if(gesla[i] == "pogresamluizasuareza88"){
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal.novKanal);
          
        }
        else{
          socket.emit('odgovorZahtevaGeslo', "Pridružitev v kanal " + novi + " ni bilo uspešno, ker je geslo napačno!");
        }
      }
    }
          
    if(stevko === 0){  
          
      kanaliGesla.push(novi);                       //dodam novkanal v tabelo kanalov!!! 
      gesla.push("pogresamluizasuareza88");
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
    }
  });
}


function obdelajPridruzitevKanaluGeslo(socket, kanaliGesla, gesla) {
  socket.on('pridruzitevZahtevaGeslo', function(prenos) {
    var st = 0; 
    for(var i in kanaliGesla){
      if(kanaliGesla[i] == prenos.kanal){
        st++;
        if(gesla[i] == "pogresamluizasuareza88"){
          socket.emit('odgovorZahtevaGeslo', "Izbrani kanal " + prenos.kanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev " + prenos.kanal + " ali zahtevajte kreiranje kanala z drugim imenom.");
        }
        else if(gesla[i] == prenos.geslo){
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, prenos.kanal);
        }
        else{
          socket.emit('odgovorZahtevaGeslo', "Pridružitev v kanal " + prenos.kanal + " ni bilo uspešno, ker je geslo napačno!");
        }
      }
    }
    
    if(st === 0){
      kanaliGesla.push(prenos.kanal);
      gesla.push(prenos.geslo);
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, prenos.kanal);
    }
    
    
    
  });
  //uporabnikiKanal(socket, trenutniKanal);
}





function obdelajOdjavoUporabnika(socket) {
  socket.on('disconnect', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
  
}
